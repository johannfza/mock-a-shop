FROM openjdk:15-jdk-alpine
ADD target/mock-a-shop-treedays.jar mock-a-shop-treedays.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "mock-a-shop-treedays.jar"]
