// Add to cart validation
function addToCart(ele){
    var target = ele.getAttribute("target");
    var value = document.getElementById(target).value;
    if (value < 1){
        event.preventDefault();
        alert("Cannot 0 qty to cart")
    } else {
        var data = {
            qty: value,
            product: {
                product_id: target
            }
        }
        postAddItem(data)
    }
}

// Add to cart POST Request
function postAddItem(data) {
    var customer_id = document.getElementById("customerid").value
    var url = "/api/shoppingcarts/items/add/customer/" + customer_id
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: url,
        data: JSON.stringify(data),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log("SUCCESS : ", data);
        },
        error: function (e) {
            console.log("ERROR : ", e);
        }
    });
}

// Alerts
function deleteProduct(){
    if (confirm("Deleting this product will also remove the product from all customer's shopping cart. Are you sure?")) {
        console.log(true)
    } else {
        console.log(false)
        event.preventDefault();
    }
}

function timetravel() {
    if (confirm("Ready to turn back time???")) {
        console.log(true)
    } else {
        console.log(false)
        event.preventDefault();
    }
}

function seedAllProducts(){
    if (confirm("Seeding products will also remove products customer's shopping carts. Ok?")) {
        console.log(true)
    } else {
        console.log(false)
        event.preventDefault();
    }
}

function seedAllData(){
    if (confirm("Deleting this product will also remove the product from all customer's shopping cart. Are you sure?")) {
        console.log(true)
    } else {
        console.log(false)
        event.preventDefault();
    }
}

function clearCart() {
    if (confirm("Confirm clear cart?")) {
        console.log(true)
    } else {
        console.log(false)
        event.preventDefault();
    }
}

function checkOut(){
    alert("Redirect to Checkout...")
}

