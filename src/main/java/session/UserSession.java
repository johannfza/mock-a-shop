package session;

import java.util.UUID;

public class UserSession {

    private String username;
    private int access;
    private UUID sessionId;

    public UserSession() {
        this.sessionId = UUID.randomUUID();
    }

    public UserSession(String username) {
        this.sessionId = UUID.randomUUID();
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getAccess() {
        return access;
    }

    public void setAccess(int access) {
        this.access = access;
    }

    public UUID getSessionId() {
        return sessionId;
    }

    public void setSessionId(UUID sessionId) {
        this.sessionId = sessionId;
    }

    @Override
    public String toString() {
        return "UserSession{" +
                "username='" + username + '\'' +
                ", access=" + access +
                ", sessionId=" + sessionId +
                '}';
    }
}
