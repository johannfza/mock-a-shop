package com.example.MockaShop.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

@Entity
public class LineItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonProperty("item_id")
    private UUID id;

    private int qty;

    @OneToOne
    private Product product;

    @ManyToOne
    @JoinColumn(name = "SHOPPING_CART_ID", nullable = false)
    private ShoppingCart shoppingCart;

    public LineItem() {

    }

    @JsonCreator
    public LineItem(@JsonProperty("item_id") UUID id, @JsonProperty("qty") int qty, @JsonProperty("product") Product product) {
        this.id = id;
        this.qty = qty;
        this.product = product;
    }

    @JsonCreator
    public LineItem(@JsonProperty("qty") int qty, @JsonProperty("product") Product product) {
        this.qty = qty;
        this.product = product;
    }

    public LineItem(int qty, Product product, ShoppingCart shoppingCart) {
        this.qty = qty;
        this.product = product;
        this.shoppingCart = shoppingCart;
    }

    public LineItem(UUID id, int qty, Product product, ShoppingCart shoppingCart) {
        this.id = id;
        this.qty = qty;
        this.product = product;
        this.shoppingCart = shoppingCart;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    public void setShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public BigDecimal getSubTotal() {
        return product.getPrice().multiply(new BigDecimal(qty));
    }
}
