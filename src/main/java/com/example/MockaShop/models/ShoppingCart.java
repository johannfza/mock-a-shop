package com.example.MockaShop.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class ShoppingCart {

    @Id
    private UUID id;

    @OneToMany(mappedBy = "shoppingCart")
    @JsonIgnoreProperties("shoppingCart")
    private List<LineItem> items;

    @OneToOne(mappedBy = "shoppingCart")
    @JsonIgnoreProperties("shoppingCart")
    private Customer customer;

    public ShoppingCart() {
    }

    public ShoppingCart(UUID id, List<LineItem> items, Customer customer) {
        this.id = id;
        this.items = items;
        this.customer = customer;
    }

    public ShoppingCart(UUID id, Customer customer) {
        this.id = id;
        this.items = new ArrayList<LineItem>();
        this.customer = customer;
    }

    public ShoppingCart(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public List<LineItem> getItems() {
        return items;
    }

    public void setItems(List<LineItem> items) {
        this.items = items;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public boolean containsProduct(Product product) {
        boolean itemInList = false;
        for (int i = 0; i < this.getItems().size(); i++) {
            if (this.items.get(i).getProduct().getId() == product.getId()) {
                itemInList = true;
                break;
            }
        }
        return itemInList;
    }

    public void updateCart(List<LineItem> newItems) {
        this.items = newItems;
    }

    public BigDecimal getTotal() {
        BigDecimal total = items.stream().map(item -> item.getSubTotal()).reduce(BigDecimal.ZERO, BigDecimal::add);
        return total;
    }

    public void deleteItem(UUID itemid) {
        boolean itemInList = false;
        for (int i = 0; i < this.items.size(); i++) {
            if (items.get(i).getProduct().getId() == itemid) {
                items.remove(i);
                itemInList = true;
            }
        }
    }

    public boolean isEmpty() {
        if (items.size() > 0) {
            return false;
        }
        return true;
    }


    public void clearCart() {
        this.items.clear();
    }


}
