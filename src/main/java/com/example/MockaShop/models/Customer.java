package com.example.MockaShop.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.UUID;

@Entity
public class Customer {

    @Id
    @JsonProperty("id")
    private UUID id;
    @JsonProperty("access")
    private final int access = 2;
    @NotBlank
    @JsonProperty("username")
    private String username;

    @JsonProperty("shoppingCart")
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "SHOPPING_CART_ID", referencedColumnName = "ID")
    private ShoppingCart shoppingCart;

    public Customer() {
        super();
    }

    public Customer(UUID id, @NotBlank String username) {
        this.id = id;
        this.username = username;
    }

    public Customer(UUID id, @NotBlank String username, ShoppingCart shoppingCart) {
        this.id = id;
        this.username = username;
        this.shoppingCart = shoppingCart;
    }

    public Customer(@NotBlank String username) {
        this.id = UUID.randomUUID();
        this.username = username;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public int getAccess() {
        return access;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    public void setShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }
}

