package com.example.MockaShop.seed;

import com.example.MockaShop.models.Customer;
import com.example.MockaShop.models.LineItem;
import com.example.MockaShop.models.Product;
import com.example.MockaShop.models.ShoppingCart;
import com.example.MockaShop.repositories.CustomerRepository;
import com.example.MockaShop.repositories.LineItemRepository;
import com.example.MockaShop.repositories.ProductRepository;
import com.example.MockaShop.repositories.ShoppingCartRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
public class DatabaseSeeder {

    private static final Logger log = LoggerFactory.getLogger(DatabaseSeeder.class);
    private ProductRepository productRepository;
    private CustomerRepository customerRepository;
    private LineItemRepository lineItemRepository;
    private ShoppingCartRepository shoppingCartRepository;

    @Autowired
    public DatabaseSeeder(ProductRepository productRepository, CustomerRepository customerRepository, LineItemRepository lineItemRepository, ShoppingCartRepository shoppingCartRepository) {
        this.productRepository = productRepository;
        this.customerRepository = customerRepository;
        this.lineItemRepository = lineItemRepository;
        this.shoppingCartRepository = shoppingCartRepository;
    }

    @EventListener
    public void seed(ContextRefreshedEvent event) {
        lineItemRepository.deleteAll();
        productRepository.deleteAll();
        customerRepository.deleteAll();
        shoppingCartRepository.deleteAll();
        seedProducts();
        seedCart();
    }

    public void seedAllOnDemand() {
        lineItemRepository.deleteAll();
        productRepository.deleteAll();
        customerRepository.deleteAll();
        shoppingCartRepository.deleteAll();
        seedProducts();
        seedCart();
    }

    private void seedProducts() {
        log.info("Clearing existing carts...");
        lineItemRepository.deleteAll();
        log.info("Seeding Products...");
        productRepository.deleteAll();
        List<Product> productSeed = new ArrayList<Product>();
        productSeed.add(new Product(UUID.fromString("4dd76282-f38c-4bc9-8d0c-e3964849dd95"), "Californication Vinyl by RHCP", "The Red Hot Chilli Peppers of the quadruple-platinum smash Blood Sugar Sex Magic returned revitalised and reunited with Californication four years later", "", BigDecimal.valueOf(39.95)));
        productSeed.add(new Product(UUID.fromString("4299255c-8343-427d-a711-b54a68a42875"), "Demon Days Vinyl by Gorillaz", "'The second studio album by the English virtual band. Features the singles ''Feel Good Inc.'', ''Dare'' and ''Dirty Harry''.'", "", BigDecimal.valueOf(54.15)));
        productSeed.add(new Product(UUID.fromString("2eb0fcf0-4f9e-4278-b74e-df52d4dd6716"), "Random Access Memories Vinyl by Daft Punk", "Random Access Memories is the new album from Daft Punk. Collaborators include Nile Rodgers, Chilly Gonzalez, Giorgio Moroder, Julian Casablancas, Pharrell Williams, Paul Williams, Todd Edwards, Panda Bear and DJ Falcon.", "", BigDecimal.valueOf(29.87)));
        productSeed.add(new Product(UUID.fromString("f9faf268-cc3a-4501-b60d-61b7d0405d14"), "Future Nostalgia Vinyl by Dua Lipa", "Dua Lipa''s highly anticipated new album 'Future Nostalgia'", "", BigDecimal.valueOf(39.95)));
        productSeed.add(new Product(UUID.fromString("a40127b0-80ac-4d14-917d-7c882c774523"), "Kind Of Blue Vinyl by Miles Davis", "Miles said that he had wanted to draw closer to African and Gospel music as well as the blues, but admitted that he had failed in this intention.", "", BigDecimal.valueOf(49.97)));
        productSeed.add(new Product(UUID.fromString("018df2a6-deea-4517-a349-59ae174d6cec"), "Blonde on Blonde Vinyl by Bob Dylan", "Rock''s first great double-album and home to many of Dylan''s finest songs.", "", BigDecimal.valueOf(45.50)));
        productSeed.add(new Product(UUID.fromString("26b3ade0-30cf-4714-ab0f-306147a151fc"), "Thriller Vinyl by Micheal Jackson", "Two vinyl LP pressing of the 25th anniversary edition of Michael Jackson''s Thriller celebrates the groundbreaking album with eight bonus tracks, five of them previously unreleasedst", "", BigDecimal.valueOf(39.90)));
        productSeed.add(new Product(UUID.fromString("6c295528-1172-45b1-a3b7-731ace6484fe"), "Dark Side of the Moon Vinyl by Pink Floyd", "This album needs little introduction as I’m sure we’ve all heard the hits and the stories that it pairs perfectly with The Wizard of Oz.", "", BigDecimal.valueOf(39.95)));
        productRepository.saveAll(productSeed);
    }

    private void seedCart() {
        log.info("Clearing exiting carts...");
        shoppingCartRepository.deleteAll();
        log.info("Seeding Carts...");
        ShoppingCart shoppingCart_John = new ShoppingCart(UUID.fromString("ff371858-4be8-408a-812c-6d63886ab2e6"));
        ShoppingCart shoppingCart_May = new ShoppingCart(UUID.fromString("898a7400-4201-4db7-8024-8d4791365f63"));
        ShoppingCart shoppingCart_Ahmad = new ShoppingCart(UUID.fromString("644f9a81-78fe-44c3-b5be-f969662da584"));
        ShoppingCart shoppingCart_Mutu = new ShoppingCart(UUID.fromString("8a0359dc-3975-4a6c-bf31-d09a1f50ec48"));
        shoppingCartRepository.saveAndFlush(shoppingCart_John);
        shoppingCartRepository.saveAndFlush(shoppingCart_May);
        shoppingCartRepository.saveAndFlush(shoppingCart_Ahmad);
        shoppingCartRepository.saveAndFlush(shoppingCart_Mutu);
        List<LineItem> itemsSeed = new ArrayList<LineItem>();
        itemsSeed.add(new LineItem(UUID.fromString("cde7f852-1967-432d-a531-7c4daed0bcaf"), 2, productRepository.findById(UUID.fromString("4dd76282-f38c-4bc9-8d0c-e3964849dd95")).get(), shoppingCart_John));
        itemsSeed.add(new LineItem(UUID.fromString("f9a2cd7e-091e-4bc2-87b8-35671ced5e59"), 3, productRepository.findById(UUID.fromString("018df2a6-deea-4517-a349-59ae174d6cec")).get(), shoppingCart_John));
        itemsSeed.add(new LineItem(UUID.fromString("301e7d16-c6a1-467f-9375-b5b903115195"), 4, productRepository.findById(UUID.fromString("6c295528-1172-45b1-a3b7-731ace6484fe")).get(), shoppingCart_John));
        lineItemRepository.saveAll(itemsSeed);

        log.info("Seeding Customers...");
        List<Customer> customerSeed = new ArrayList<>();
        customerSeed.add(new Customer(UUID.fromString("6b83444c-5a9c-41ce-9785-53e4a582c4f9"), "John", shoppingCart_John));
        customerSeed.add(new Customer(UUID.fromString("d9f3ef82-2829-4d77-8f1b-e19c30d82bc6"), "May", shoppingCart_May ));
        customerSeed.add(new Customer(UUID.fromString("04b0114f-db09-40b1-b451-d960eb0eba64"), "Ahmad", shoppingCart_Ahmad));
        customerSeed.add(new Customer(UUID.fromString("3d0d495b-c098-46be-aaa5-3a3f2ee1dbf2"), "Mutu", shoppingCart_Mutu));
        customerRepository.saveAll(customerSeed);

    }
}
