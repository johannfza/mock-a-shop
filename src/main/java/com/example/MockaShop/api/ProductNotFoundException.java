package com.example.MockaShop.api;

import java.util.UUID;

class ProductNotFoundException extends RuntimeException {

    ProductNotFoundException(UUID id) {
        super("Could not find product with id: " + id);
    }
}