package com.example.MockaShop.api;

import com.example.MockaShop.models.Customer;
import com.example.MockaShop.services.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.text.MessageFormat;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/* REST Api Controller for Customers */
@RestController
@RequestMapping("api/customers")
public class CustomerApiController {

    private static final Logger log = LoggerFactory.getLogger(CustomerApiController.class);

    private final CustomerService customerService;

    @Autowired
    CustomerApiController(CustomerService customerService) {
        this.customerService = customerService;
    }

    /* GET: Get all customers */
    @GetMapping
    List<Customer> getAll() {
        return customerService.getAll();
    }

    /* POST: Create new customer */
    @PostMapping
    ResponseEntity<Void> newCustomer(@Valid @RequestBody Customer customer) {
        Customer newCustomer = customerService.createCustomerWithUsername(customer.getUsername());
        if ( newCustomer == null ) {
            return ResponseEntity.noContent().build();
        }
        log.info("Successfully created customer with id: {}", newCustomer.getId());
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(newCustomer.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    /* GET: Get customer by id */
    @GetMapping("{id}")
    Customer getById(@PathVariable UUID id) {
        return customerService.findById(id)
                .orElseThrow(() -> new CustomerNotFoundException(id));
    }

    /* PUT: Update customer */
    @PutMapping("{id}")
    ResponseEntity<Customer> updateCustomer(@Valid @RequestBody Customer customer, @PathVariable UUID id) {
        Customer updatedCustomer;
        Optional<Customer> optionalCustomer = customerService.findById(customer.getId());
        if(optionalCustomer != null){
            updatedCustomer = customerService.save(customer);
            log.info("Successfully updated customer with id: {}", id);
        } else {
            throw new CustomerNotFoundException(id);
        }
        return ResponseEntity.ok(updatedCustomer);
    }

    /* DELETE: Delete customer */
    @DeleteMapping("{id}")
    ResponseEntity<String> deleteCustomer(@PathVariable UUID id) {
        Optional<Customer> optionalCustomer = customerService.findById(id);
        if(optionalCustomer != null){
            customerService.deleteById(id);
        } else {
            throw new CustomerNotFoundException(id);
        }
        log.info("Successfully deleted customer with id: {}", id);
        return ResponseEntity.ok(MessageFormat.format("Successfully deleted customer with id: {0}", id ));
    }

}
