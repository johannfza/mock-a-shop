package com.example.MockaShop.api;

import com.example.MockaShop.models.ShoppingCart;
import com.example.MockaShop.models.LineItem;
import com.example.MockaShop.services.ShoppingCartService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.persistence.Entity;
import javax.validation.Valid;
import java.net.URI;
import java.text.MessageFormat;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/* REST Api Controller for ShoppingCarts */
@RestController
@RequestMapping("api/shoppingcarts")
public class ShoppingCartApiController {

    private static final Logger log = LoggerFactory.getLogger(ProductApiController.class);

    private final ShoppingCartService shoppingCartService;

    ShoppingCartApiController(ShoppingCartService shoppingCartService) {
        this.shoppingCartService = shoppingCartService;
    }

    /* GET: Get all shopping carts */
    @GetMapping
    ResponseEntity<List<ShoppingCart>> getAll() {
        return ResponseEntity.ok(shoppingCartService.getAll());
    }

    /* GET: Get shopping Cart by customer id */
    @GetMapping("/customer/{id}")
    ResponseEntity<ShoppingCart> getByCustomerId(@PathVariable UUID id) {
        ShoppingCart shoppingCart = shoppingCartService.findByCustomerId(id);
        if (shoppingCart == null) {
            throw new ShoppingCartNotFoundException(id);
        }
        return ResponseEntity.ok(shoppingCart);
    }

    /* GET: Get shopping Cart by cart id */
    @GetMapping("{id}")
    ResponseEntity<ShoppingCart> getByCartId(@PathVariable UUID id) {
        Optional<ShoppingCart> shoppingCart = shoppingCartService.findById(id);
        if (shoppingCart == null) {
            throw new ShoppingCartNotFoundException(id);
        }
        return ResponseEntity.ok(shoppingCart.get());
    }

    /* PUT: Update shopping Cart by cart id */
    @PutMapping("{id}")
    ResponseEntity<ShoppingCart> updateShoppingCart(@Valid @RequestBody ShoppingCart shoppingCart, @PathVariable UUID id) {
        ShoppingCart updatedShoppingCart;
        Optional<ShoppingCart> optionalShoppingCart = shoppingCartService.findById(shoppingCart.getId());
        if (optionalShoppingCart != null) {
            updatedShoppingCart = shoppingCartService.save(shoppingCart);
            log.info("Successfully updated shoppingCart with id: {}", id);
        } else {
            throw new ShoppingCartNotFoundException(id);
        }
        return ResponseEntity.ok(updatedShoppingCart);
    }

    /* PUT: Clear cart */
    @PutMapping("/clear/{id}")
    ResponseEntity<ShoppingCart> clearShoppingCart(@PathVariable UUID id) {
        ShoppingCart updatedShoppingCart;
        Optional<ShoppingCart> optionalShoppingCart = shoppingCartService.findById(id);
        if (optionalShoppingCart != null) {
            updatedShoppingCart = shoppingCartService.clearCart(id);
        } else {
            throw new ShoppingCartNotFoundException(id);
        }
        log.info("Successfully cleared shoppingCart with id: {}", id);
        return ResponseEntity.ok(updatedShoppingCart);
    }

    /* Endpoints for LineItems */

    /* POST: Add item to cart by cart id */
    @PostMapping("/items/add/{id}")
    ResponseEntity<ShoppingCart> addItemToCart(@RequestBody LineItem lineItem, @PathVariable UUID id) {
        ShoppingCart shoppingCart = shoppingCartService.addItem(lineItem, id);
        if (shoppingCart == null) {
            throw new ShoppingCartNotFoundException(id);
        }
        log.info("Successfully add LineItem to cart id: {}", shoppingCart.getId());
        return ResponseEntity.ok(shoppingCart);
    }

    /* POST: Add item to cart by customer id */
    @PostMapping("/items/add/customer/{id}")
    ResponseEntity<ShoppingCart> addItemToCartByCustomerId(@RequestBody LineItem lineItem, @PathVariable UUID id) {
        ShoppingCart shoppingCart = shoppingCartService.addItemByCustomerId(lineItem, id);
        if (shoppingCart == null) {
            throw new ShoppingCartNotFoundException(id);
        }
        log.info("Successfully add LineItem to cart id: {}", shoppingCart.getId());
        return ResponseEntity.ok(shoppingCart);
    }

    /* DELETE: Delete item from cart with cart id */
    @DeleteMapping("/items/{item_id}/delete/{id}")
    ResponseEntity<String> removeItemFromCart(@PathVariable UUID item_id, @PathVariable UUID id) {
        ShoppingCart shoppingCart = shoppingCartService.removeItemFromCart(item_id, id);
        if (shoppingCart == null) {
            throw new ShoppingCartNotFoundException(id);
        }
        log.info("Successfully deleted item {} in cart id: {}", item_id, id);
        return ResponseEntity.ok(MessageFormat.format("Successfully deleted lineItem {0} shoppingCart with id: {1}", item_id, id));
    }

}
