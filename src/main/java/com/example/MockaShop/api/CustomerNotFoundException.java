package com.example.MockaShop.api;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.UUID;

class CustomerNotFoundException extends RuntimeException {

    CustomerNotFoundException(UUID id) {
        super("Could not find customer with id: " + id);
    }
}