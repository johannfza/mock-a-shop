package com.example.MockaShop.api;

import java.util.UUID;

class ShoppingCartNotFoundException extends RuntimeException {

    ShoppingCartNotFoundException(UUID id) {
        super("Could not find ShoppingCart with id: " + id);
    }
}