package com.example.MockaShop.repositories;

import com.example.MockaShop.models.LineItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface LineItemRepository extends JpaRepository<LineItem, UUID> {

    List<LineItem> findAllByProductId(UUID id);
    List<LineItem> findAllByShoppingCartId(UUID id);

}
