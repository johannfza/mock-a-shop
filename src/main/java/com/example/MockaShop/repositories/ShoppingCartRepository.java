package com.example.MockaShop.repositories;

import com.example.MockaShop.models.ShoppingCart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ShoppingCartRepository extends JpaRepository<ShoppingCart, UUID> {

    ShoppingCart findByCustomerId(UUID id);

}

