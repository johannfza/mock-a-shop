package com.example.MockaShop;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MockAShopApplication {

    private static final Logger log = LoggerFactory.getLogger(MockAShopApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(MockAShopApplication.class, args);
    }
}

