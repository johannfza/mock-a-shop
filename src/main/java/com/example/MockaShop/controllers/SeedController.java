package com.example.MockaShop.controllers;

import com.example.MockaShop.seed.DatabaseSeeder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/seed")
public class SeedController {

    private DatabaseSeeder databaseSeeder;

    @Autowired
    private SeedController(DatabaseSeeder databaseSeeder) {
        this.databaseSeeder = databaseSeeder;
    }

    @GetMapping("/timetravel")
    public String reSeed() {
        databaseSeeder.seedAllOnDemand();
        return "redirect:/";
    }
}
