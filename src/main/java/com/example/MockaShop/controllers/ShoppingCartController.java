package com.example.MockaShop.controllers;

import com.example.MockaShop.models.ShoppingCart;
import com.example.MockaShop.repositories.LineItemRepository;
import com.example.MockaShop.services.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttribute;
import session.UserSession;

import java.util.UUID;

@Controller
@RequestMapping("/cart")
public class ShoppingCartController {

    private ShoppingCartService shoppingCartService;
    private LineItemRepository lineItemRepository;

    @Autowired
    private ShoppingCartController(ShoppingCartService shoppingCartService, LineItemRepository lineItemRepository) {
        this.shoppingCartService = shoppingCartService;
        this.lineItemRepository = lineItemRepository;
    }

    @GetMapping
    public String getCart(@SessionAttribute("user") UserSession user, Model model) {
        if (user.getAccess() != 2 || user.getSessionId() == null) {
            return "redirect:/notfound";
        }
        model.addAttribute("welcome", "Welcome " + user.getUsername());
        ShoppingCart shoppingCart = shoppingCartService.findByCustomerId(user.getSessionId());
        model.addAttribute("cart", shoppingCart);
        model.addAttribute("total", "Total: $" + shoppingCart.getTotal());
        return "shoppingcart";
    }

    @GetMapping("/remove/{id}")
    public String removeItem(Model model, @PathVariable("id") UUID id) {
        lineItemRepository.deleteById(id);
        return "redirect:/cart";
    }

    @GetMapping("/clearcart/{id}")
    public String clearCart(@PathVariable("id") UUID id) {
        shoppingCartService.clearCart(id);
        return "redirect:/cart";
    }
}