package com.example.MockaShop.controllers;

import com.example.MockaShop.models.Customer;
import com.example.MockaShop.models.ShoppingCart;
import com.example.MockaShop.services.CustomerService;
import com.example.MockaShop.services.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import session.UserSession;
import java.util.UUID;

@Controller
@SessionAttributes("user")
public class HomeController {

    private ShoppingCartService shoppingCartService;
    private CustomerService customerService;

    @Autowired
    private HomeController(ShoppingCartService shoppingCartService, CustomerService customerService) {
        this.shoppingCartService = shoppingCartService;
        this.customerService = customerService;
    }

    @ModelAttribute("user")
    public UserSession setUser() {
        return new UserSession();
    }

    @RequestMapping("/")
    public String index() {
        return "index";
    }

    @RequestMapping("/admin")
    public String toAdminister(@SessionAttribute("user") UserSession user) {
        user.setUsername("Shop Owner");
        user.setAccess(1);
        user.setSessionId(UUID.randomUUID());
        System.out.println(user);
        return "redirect:/products";
    }

    @RequestMapping("/customer/{id}")
    public String johnLogin(@SessionAttribute("user") UserSession user, @PathVariable("id") UUID id) {
        Customer customer = customerService.findById(id).get();
        if (customer == null) {
            return "redirect:/notfound";
        }
        user.setUsername(customer.getUsername());
        user.setAccess(2);
        user.setSessionId(customer.getId());
        System.out.println(user);
        ShoppingCart shoppingCart = shoppingCartService.findByCustomerId(customer.getId());
        if (shoppingCart == null) {
            ShoppingCart newShoppingCart = new ShoppingCart(user.getSessionId(), null, customer);
            shoppingCartService.save(newShoppingCart);
        }
        return "redirect:/products";
    }

}