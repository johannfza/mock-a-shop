package com.example.MockaShop.controllers;

import com.example.MockaShop.models.Product;
import com.example.MockaShop.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import session.UserSession;

import javax.validation.Valid;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Controller
@RequestMapping("/products")
public class ProductController {

    private ProductService productService;

    @Autowired
    private ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public String findProducts(@SessionAttribute("user") UserSession user, Model model, @RequestParam(defaultValue = "") String query) {
        if (user.getAccess() == 0 || user.getAccess() == 1 || user.getAccess() == 2) {
            model.addAttribute("access", user.getAccess());
            model.addAttribute("customerid", user.getSessionId());
            model.addAttribute("welcome", "Welcome " + user.getUsername());
            List<Product> products = productService.findProductByName(query);
            model.addAttribute("products", products);
            return "products";
        }
        return "redirect:/";
    }

    @GetMapping("/add")
    public String addProduct(Model model) {
        model.addAttribute("access", 1);
        Product product = new Product();
        model.addAttribute("product", product);
        return "productform";
    }

    @PostMapping("/add")
    public String addProduct(@Valid @ModelAttribute Product product, BindingResult bindingResult) throws GeneralSecurityException {
        if (bindingResult.hasErrors()) {
            bindingResult.getFieldErrors().stream()
                    .forEach(f -> System.out.println(f.getField() + ": " + f.getDefaultMessage()));
            return "redirect:/products/add";
        }
        product.setId(UUID.randomUUID());
        productService.save(product);
        List<Product> products = productService.getAll();
        return "redirect:/products";
    }

    @GetMapping("/edit/{id}")
    public String editProductForm(Model model, @PathVariable("id") UUID id) {
        model.addAttribute("access", 1);
        Optional<Product> Optproduct = productService.findById(id);
        if (!Optproduct.isPresent()) {
            return "notfound";
        }
        model.addAttribute("product", Optproduct.get());
        return "productform";
    }

    @PostMapping("/edit/{id}")
    public String editProduct(@Valid @ModelAttribute Product product, BindingResult bindingResult, @PathVariable("id") UUID id) {
        if (bindingResult.hasErrors()) {
            bindingResult.getFieldErrors().stream()
                    .forEach(f -> System.out.println(f.getField() + ": " + f.getDefaultMessage()));
            return "redirect:/products/edit";
        }
        productService.save(product);
        List<Product> products = productService.getAll();
        return "redirect:/products";
    }

    @GetMapping("/delete/{id}")
    public String editProduct(Model model, @PathVariable("id") UUID id) {
        model.addAttribute("access", 1);
        Optional<Product> Optproduct = productService.findById(id);
        if (!Optproduct.isPresent()) {
            return "Not Found";
        }
        productService.delete(Optproduct.get());
        return "redirect:/products";
    }
}

