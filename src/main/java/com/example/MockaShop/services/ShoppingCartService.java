package com.example.MockaShop.services;

import com.example.MockaShop.models.Customer;
import com.example.MockaShop.models.LineItem;
import com.example.MockaShop.models.Product;
import com.example.MockaShop.models.ShoppingCart;
import com.example.MockaShop.repositories.CustomerRepository;
import com.example.MockaShop.repositories.LineItemRepository;
import com.example.MockaShop.repositories.ProductRepository;
import com.example.MockaShop.repositories.ShoppingCartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ShoppingCartService {

    private ShoppingCartRepository shoppingCartRepository;
    private LineItemRepository lineItemRepository;
    private ProductRepository productRepository;
    private CustomerRepository customerRepository;

    @Autowired
    public ShoppingCartService(ShoppingCartRepository shoppingCartRepository, LineItemRepository lineItemRepository, ProductRepository productRepository, CustomerRepository customerRepository) {
        this.shoppingCartRepository = shoppingCartRepository;
        this.lineItemRepository = lineItemRepository;
        this.productRepository = productRepository;
        this.customerRepository = customerRepository;
    }

    public List<ShoppingCart> getAll() {
        return shoppingCartRepository.findAll();
    }

    public Optional<ShoppingCart> findById(UUID id) {
        return shoppingCartRepository.findById(id);
    }

    public ShoppingCart save(ShoppingCart shoppingCart) {
        return shoppingCartRepository.saveAndFlush(shoppingCart);
    }

    public ShoppingCart saveWithCustomerId(UUID customerid) {
        Optional<Customer> optionalCustomer = customerRepository.findById(customerid);
        if (optionalCustomer == null) {
            return null;
        } else {
            ShoppingCart newShoppingCart = new ShoppingCart(UUID.randomUUID(), new ArrayList<LineItem>(), optionalCustomer.get());
            return shoppingCartRepository.saveAndFlush(newShoppingCart);
        }
    }

    public void delete(ShoppingCart shoppingCart) {
        shoppingCart.getItems().forEach(item ->
                lineItemRepository.delete(item));
        shoppingCartRepository.delete(shoppingCart);
    }

    public void deleteById(UUID id) {
        Optional<ShoppingCart> shoppingCart = shoppingCartRepository.findById(id);
        shoppingCart.get().getItems().forEach(item ->
                lineItemRepository.delete(item));
        shoppingCartRepository.deleteById(id);
    }

    public ShoppingCart findByCustomerId(UUID id) {
        return shoppingCartRepository.findByCustomerId(id);
    }

    public ShoppingCart addItemByCustomerId(LineItem lineItem, UUID customerId) {
        // Find Shopping Cart else return [null]
        ShoppingCart shoppingCart = shoppingCartRepository.findByCustomerId(customerId);
        if (shoppingCart == null) {
            return null;
        }
        // Check for product id else return [null]
        Optional<Product> optProduct = productRepository.findById(lineItem.getProduct().getId());
        if (!optProduct.isPresent()) {
            System.out.println("No such product");
            return null;
        }
        // Set Product variable
        Product product = optProduct.get();
        // Persist data
        return persistItem(shoppingCart, lineItem, product);
    }

    public ShoppingCart addItem(LineItem lineItem, UUID id) {
        // Find Shopping Cart else return [null]
        Optional<ShoppingCart> optShoppingCart = shoppingCartRepository.findById(id);
        if (!optShoppingCart.isPresent()) {
            return optShoppingCart.orElse(null);
        }
        // Check for product id else return [null]
        Optional<Product> optProduct = productRepository.findById(lineItem.getProduct().getId());
        if (!optProduct.isPresent()) {
            System.out.println("No such product");
            return null;
        }
        // Set shopping cart and product variable
        ShoppingCart shoppingCart = optShoppingCart.get();
        Product product = optProduct.get();
        // Persist data
        return persistItem(shoppingCart, lineItem, product);
    }

    public ShoppingCart updateCart(List<LineItem> newItems, UUID id) {
        Optional<ShoppingCart> optShoppingCart = shoppingCartRepository.findById(id);
        if (!optShoppingCart.isPresent()) {
            return optShoppingCart.orElse(null);
        }
        optShoppingCart.get().updateCart(newItems);
        shoppingCartRepository.save(optShoppingCart.get());
        ShoppingCart updatedShoppingCart = shoppingCartRepository.findById(id).orElse(null);
        return updatedShoppingCart;
    }

    public ShoppingCart removeItemFromCart(UUID lineItemId, UUID CartTd) {
        Optional<ShoppingCart> optShoppingCart = shoppingCartRepository.findById(CartTd);
        if (!optShoppingCart.isPresent()) {
            return optShoppingCart.orElse(null);
        }
        optShoppingCart.get().deleteItem(lineItemId);
        shoppingCartRepository.save(optShoppingCart.get());
        ShoppingCart updatedShoppingCart = shoppingCartRepository.findById(CartTd).orElse(null);
        return updatedShoppingCart;
    }

    public ShoppingCart clearCart(UUID id) {
        Optional<ShoppingCart> optShoppingCart = shoppingCartRepository.findById(id);
        if (!optShoppingCart.isPresent()) {
            return optShoppingCart.orElse(null);
        }
        List<LineItem> items = lineItemRepository.findAllByShoppingCartId(id);
        lineItemRepository.deleteAll(items);
        ShoppingCart updatedShoppingCart = shoppingCartRepository.findById(id).orElse(null);
        return updatedShoppingCart;
    }

    public ShoppingCart persistItem(ShoppingCart shoppingCart, LineItem lineItem, Product product) {
        ShoppingCart updatedShoppingCart = new ShoppingCart();
        // Check if found cart contains product.
        if (!shoppingCart.containsProduct(product)) {
            // If product not found add new item
            System.out.println("Cart does not contain product");
            LineItem tempLineItem = new LineItem(lineItem.getQty(), product, shoppingCart);
            System.out.println("Adding new product to cart...");
            lineItemRepository.saveAndFlush(tempLineItem);
            shoppingCart.getItems().add(tempLineItem);
            updatedShoppingCart = shoppingCart;
        } else {
            // If product already in list increment qty
            System.out.println("Current cart contains product...");
            for (int i = 0; i < shoppingCart.getItems().size(); i++) {
                if (shoppingCart.getItems().get(i).getProduct().getId() == lineItem.getProduct().getId()) {
                    System.out.println("Incrementing...");
                    LineItem tempLineItem = new LineItem(shoppingCart.getItems().get(i).getId(), shoppingCart.getItems().get(i).getQty() + lineItem.getQty(), product, shoppingCart);
                    lineItemRepository.saveAndFlush(tempLineItem);
                    // Save update shopping cart
                    updatedShoppingCart = shoppingCartRepository.findById(shoppingCart.getId()).get();
                }
            }
        }
        return updatedShoppingCart;
    }
}

