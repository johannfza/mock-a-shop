package com.example.MockaShop.services;

import com.example.MockaShop.models.Customer;
import com.example.MockaShop.models.LineItem;
import com.example.MockaShop.models.Product;
import com.example.MockaShop.repositories.LineItemRepository;
import com.example.MockaShop.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProductService {

    private ProductRepository productRepository;
    private LineItemRepository lineItemRepository;

    @Autowired
    public ProductService(ProductRepository productRepository, LineItemRepository lineItemRepository){
        this.productRepository = productRepository;
        this.lineItemRepository = lineItemRepository;
    }

    public List<Product> getAll() {
        return (List<Product>) productRepository.findAll();
    }

    public Optional<Product> findById(UUID id){
        return productRepository.findById(id);
    }

    public List<Product> findProductByName(String s){
        return productRepository.findProductByNameContainingIgnoreCase(s);
    }

    public Product save(Product product){
        return productRepository.saveAndFlush(product);
    }

    public void delete(Product product){
        List<LineItem> lineItems = lineItemRepository.findAllByProductId(product.getId());
        if (lineItems.size() > 0) {
            lineItemRepository.deleteAll(lineItems);
        }
        productRepository.delete(product);
    }

    public void deleteById(UUID id){
        List<LineItem> lineItems = lineItemRepository.findAllByProductId(id);
        if (lineItems.size() > 0) {
            lineItemRepository.deleteAll(lineItems);
        }
        productRepository.deleteById(id);
    }

}
