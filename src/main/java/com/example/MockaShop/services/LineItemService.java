package com.example.MockaShop.services;

import com.example.MockaShop.models.LineItem;
import com.example.MockaShop.models.Product;
import com.example.MockaShop.repositories.LineItemRepository;
import com.example.MockaShop.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class LineItemService {

    private LineItemRepository lineItemRepository;

    @Autowired
    public LineItemService(LineItemRepository lineItemRepository){
        this.lineItemRepository = lineItemRepository;
    }

    public List<LineItem> getAll() {
        return (List<LineItem>) lineItemRepository.findAll();
    }

    public Optional<LineItem> findById(UUID id){
        return lineItemRepository.findById(id);
    }

    public LineItem save(LineItem lineItem){
        return lineItemRepository.saveAndFlush(lineItem);
    }

    public void delete(LineItem lineItem){
        lineItemRepository.delete(lineItem);
    }

    public List<LineItem> findByShoppingCartId(UUID id){
        return lineItemRepository.findAllByShoppingCartId(id);
    }

}