package com.example.MockaShop.services;

import com.example.MockaShop.models.Customer;
import com.example.MockaShop.models.LineItem;
import com.example.MockaShop.models.ShoppingCart;
import com.example.MockaShop.repositories.CustomerRepository;
import com.example.MockaShop.repositories.LineItemRepository;
import com.example.MockaShop.repositories.ProductRepository;
import com.example.MockaShop.repositories.ShoppingCartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CustomerService {

    private CustomerRepository customerRepository;
    private LineItemRepository lineItemRepository;
    private ShoppingCartRepository shoppingCartRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository, LineItemRepository lineItemRepository,ShoppingCartRepository shoppingCartRepository){
        this.customerRepository = customerRepository;
        this.lineItemRepository = lineItemRepository;
        this.shoppingCartRepository = shoppingCartRepository;
    }

    public List<Customer> getAll() {
        return (List<Customer>) customerRepository.findAll();
    }

    public Optional<Customer> findById(UUID id){
        return customerRepository.findById(id);
    }

    public Customer save(Customer customer){
        return customerRepository.saveAndFlush(customer);
    }

    public Customer createCustomerWithUsername(String username){
        ShoppingCart shoppingCart = new ShoppingCart(UUID.randomUUID());
        shoppingCartRepository.save(shoppingCart);
        Customer newCustomer = new Customer(UUID.randomUUID(), username, shoppingCart);
        return customerRepository.saveAndFlush(newCustomer);
    }

    public void delete(Customer customer){
        customer.getShoppingCart().getItems().forEach(item ->
                lineItemRepository.delete(item));
        customerRepository.deleteById(customer.getId());
        shoppingCartRepository.delete(customer.getShoppingCart());
    }

    public void deleteById(UUID id){
        Customer customer = customerRepository.findById(id).get();
        customer.getShoppingCart().getItems().forEach(item ->
                lineItemRepository.delete(item));
        customerRepository.deleteById(id);
        shoppingCartRepository.delete(customer.getShoppingCart());
    }
}
