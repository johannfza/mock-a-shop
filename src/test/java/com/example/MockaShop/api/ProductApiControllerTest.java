package com.example.MockaShop.api;

import com.example.MockaShop.models.Product;
import com.example.MockaShop.services.ProductService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ProductApiController.class)
public class ProductApiControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    ProductService productService;

    @Autowired
    ObjectMapper mapper;

    Product product1;
    Product product2;
    Product product3;

    List<Product> products;

    String endpoint = "/api/products";

    /* Setup test data */
    @Before
    public void setup() throws JsonProcessingException {
        product1 = new Product(UUID.fromString("4dd76282-f38c-4bc9-8d0c-e3964849dd95"), "Product 1", "About Product 1", "", BigDecimal.valueOf(10));
        product2 = new Product(UUID.fromString("4299255c-8343-427d-a711-b54a68a42875"), "Product 2", "About Product 2", "", BigDecimal.valueOf(20));
        product3 = new Product(UUID.fromString("2eb0fcf0-4f9e-4278-b74e-df52d4dd6716"), "Product 3", "About Product 3", "", BigDecimal.valueOf(30));

        products = new ArrayList<>();
        products.add(product1);
        products.add(product2);
        products.add(product3);
    }

    /* Test - GET: Get all products
    Expected Response:
    Status = 200
    Body = [{"product_id":"4dd76282-f38c-4bc9-8d0c-e3964849dd95","name":"Product 1","description":"About Product 1","imgUrl":"","price":10},
            {"product_id":"4299255c-8343-427d-a711-b54a68a42875","name":"Product 2","description":"About Product 2","imgUrl":"","price":20},
            {"product_id":"2eb0fcf0-4f9e-4278-b74e-df52d4dd6716","name":"Product 3","description":"About Product 3","imgUrl":"","price":30}]
    */
    @Test
    public void getAll() throws Exception {
        // given
        Mockito.when(productService.getAll()).thenReturn(products);

        // when
        mockMvc.perform(MockMvcRequestBuilders.get(endpoint))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.content().json(mapper.writeValueAsString(products)))
                .andExpect(MockMvcResultMatchers.status().isOk());

        // then
        Mockito.verify(productService).getAll();
    }

    /* Test - POST: Create new product
    Expected Response:
    Status = 201
    Headers = [Location:"http://localhost/api/products/e450fda3-0747-4429-bb64-94025b6e0b11"]
    */
    @Test
    public void newProduct() throws Exception {
        // given
        JSONObject input = new JSONObject();
        input.put("name", "New Product");
        input.put("price", 10.50);
        Product savedProduct = new Product(UUID.fromString("e450fda3-0747-4429-bb64-94025b6e0b11"), "New Product", "About New Product", "", BigDecimal.valueOf(10.50));
        Mockito.when(productService.save(Mockito.any())).thenReturn(savedProduct);

        // when
        mockMvc.perform(MockMvcRequestBuilders.post(endpoint)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .content(input.toString()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.header().string("Location", "http://localhost/api/products/e450fda3-0747-4429-bb64-94025b6e0b11"));

        // then
        Mockito.verify(productService).save(Mockito.any());
    }

    /* Test - GET: Get product by id
    Expected Response:
    Status = 200
    Body = {"product_id":"4dd76282-f38c-4bc9-8d0c-e3964849dd95","name":"Product 1","description":"About Product 1","imgUrl":"","price":10}
    */
    @Test
    public void getById() throws Exception {
        // given
        Mockito.when(productService.findById(product1.getId())).thenReturn(Optional.of(product1));

        // when
        mockMvc.perform(MockMvcRequestBuilders.get(endpoint + "/{0}", product1.getId()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.content().json(mapper.writeValueAsString(product1)))
                .andExpect(MockMvcResultMatchers.status().isOk());

        // then
        Mockito.verify(productService).findById(product1.getId());

    }

    /* Test - PUT: Update product username
    Expected Response:
    Status = 200
    Body = {"product_id":"4dd76282-f38c-4bc9-8d0c-e3964849dd95","name":"Updated Product","description":"About Updated Product","imgUrl":"","price":10.5}
    */
    @Test
    public void updateProduct() throws Exception {
        // given
        Product updatedProduct = new Product(UUID.fromString("4dd76282-f38c-4bc9-8d0c-e3964849dd95"), "Updated Product", "About Updated Product", "", BigDecimal.valueOf(10.50));
        Mockito.when(productService.findById(updatedProduct.getId())).thenReturn(Optional.of(product1));
        Mockito.when(productService.save(Mockito.any())).thenReturn(updatedProduct);

        // when
        mockMvc.perform(MockMvcRequestBuilders.put(endpoint + "/{0}", updatedProduct.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .content(mapper.writeValueAsString(updatedProduct)))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());

        // then
        Mockito.verify(productService).findById(updatedProduct.getId());
        Mockito.verify(productService).save(Mockito.any());
    }

    /* Test - PUT: Update a product with non existing id
    Expected Response:
    Status = 404
    Body = Could not find product with id: cbef38d6-a1a7-40dd-a232-e1f093ba8ca3
    */
    @Test
    public void updateNotFoundProduct() throws Exception {
        // given
        Product notFoundProduct = new Product(UUID.fromString("cbef38d6-a1a7-40dd-a232-e1f093ba8ca3"), "Non Existing Product", "", "", BigDecimal.valueOf(10));
        Mockito.when(productService.findById(notFoundProduct.getId())).thenReturn(null);

        // when
        mockMvc.perform(MockMvcRequestBuilders.put(endpoint + "/{0}", notFoundProduct.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .content(mapper.writeValueAsString(notFoundProduct)))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        // then
        Mockito.verify(productService).findById(notFoundProduct.getId());
    }

    /* Test - DELETE: Delete product by id
    Expected Response:
    Status = 200
    Body = Successfully deleted product with id: 2eb0fcf0-4f9e-4278-b74e-df52d4dd6716
    */
    @Test
    public void deleteProduct() throws Exception {
        // given
        Mockito.doNothing().when(productService).deleteById(product3.getId());

        // when
        mockMvc.perform(MockMvcRequestBuilders.delete(endpoint + "/{0}", product3.getId()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());

        // then
        Mockito.verify(productService).deleteById(product3.getId());
    }
}