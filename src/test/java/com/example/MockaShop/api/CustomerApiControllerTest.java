package com.example.MockaShop.api;

import com.example.MockaShop.models.Customer;
import com.example.MockaShop.services.CustomerService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CustomerApiController.class)
public class CustomerApiControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    CustomerService customerService;

    @Autowired
    ObjectMapper mapper;

    Customer john;
    Customer may;
    List<Customer> customerList;

    String endpoint = "/api/customers";

    /* Setup test data */
    @Before
    public void setup() throws JsonProcessingException {
        john = new Customer(UUID.fromString("6b83444c-5a9c-41ce-9785-53e4a582c4f9"), "John");
        may = new Customer(UUID.fromString("d9f3ef82-2829-4d77-8f1b-e19c30d82bc6"), "May");
        customerList = new ArrayList<Customer>();
        customerList.add(john);
        customerList.add(may);
    }

    /* Test - GET: Get all customers
    Expected Response:
    Status = 200
    Body = [{"id":"6b83444c-5a9c-41ce-9785-53e4a582c4f9","access":2,"username":"John","shoppingCart":null},
            {"id":"d9f3ef82-2829-4d77-8f1b-e19c30d82bc6","access":2,"username":"May","shoppingCart":null}]
    */
    @Test
    public void getAll() throws Exception {
        // given
        Mockito.when(customerService.getAll()).thenReturn(customerList);

        // when
        mockMvc.perform(MockMvcRequestBuilders.get(endpoint))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.content().json(mapper.writeValueAsString(customerList)))
                .andExpect(MockMvcResultMatchers.status().isOk());

        // then
        Mockito.verify(customerService).getAll();
    }

    /* Test - POST: Create new customer
    Expected Response:
    Status = 201
    Header = [Location: "http://localhost/api/customers/3d0d495b-c098-46be-aaa5-3a3f2ee1dbf2"]
    */
    @Test
    public void newCustomer() throws Exception {
        // given
        JSONObject input = new JSONObject();
        input.put("username", "New Customer");
        Customer savedCustomer = new Customer(UUID.fromString("3d0d495b-c098-46be-aaa5-3a3f2ee1dbf2"), "New Customer");
        Mockito.when(customerService.createCustomerWithUsername(input.get("username").toString())).thenReturn(savedCustomer);

        // when
        mockMvc.perform(MockMvcRequestBuilders.post(endpoint)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .content(input.toString()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.header().string("Location", "http://localhost/api/customers/3d0d495b-c098-46be-aaa5-3a3f2ee1dbf2"));

        // then
        Mockito.verify(customerService).createCustomerWithUsername(input.get("username").toString());
    }

    /* Test - GET: Get customer by id
    Expected Response:
    Status = 200
    Body = {"id":"6b83444c-5a9c-41ce-9785-53e4a582c4f9","access":2,"username":"John","shoppingCart":null}
    */
    @Test
    public void getById() throws Exception {
        // given
        Mockito.when(customerService.findById(john.getId())).thenReturn(Optional.of(john));

        // when
        mockMvc.perform(MockMvcRequestBuilders.get(endpoint + "/{0}", john.getId()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.content().json(mapper.writeValueAsString(john)))
                .andExpect(MockMvcResultMatchers.status().isOk());

        // then
        Mockito.verify(customerService).findById(john.getId());

    }

    /* Test - PUT: Update customer username
    Expected Response:
    Status = 200
    Body = {"id":"6b83444c-5a9c-41ce-9785-53e4a582c4f9","access":2,"username":"Updated John","shoppingCart":null}
    */
    @Test
    public void updateCustomer() throws Exception {
        // given
        Customer updatedJohn = new Customer(UUID.fromString("6b83444c-5a9c-41ce-9785-53e4a582c4f9"), "Updated John");
        Mockito.when(customerService.findById(updatedJohn.getId())).thenReturn(Optional.of(john));
        Mockito.when(customerService.save(Mockito.any())).thenReturn(updatedJohn);

        // when
        mockMvc.perform(MockMvcRequestBuilders.put(endpoint + "/{0}", updatedJohn.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .content(mapper.writeValueAsString(updatedJohn)))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());

        // then
        Mockito.verify(customerService).findById(updatedJohn.getId());
        Mockito.verify(customerService).save(Mockito.any());
    }

    /* Test - PUT: Update a with non existing id
    Expected Response:
    Status = 404
    Body = Could not find customer with id: cbef38d6-a1a7-40dd-a232-e1f093ba8ca3
    */
    @Test
    public void updateNotFoundCustomer() throws Exception {
        // given
        Customer notFoundCustomer = new Customer(UUID.fromString("cbef38d6-a1a7-40dd-a232-e1f093ba8ca3"), "Non Existing Customer");
        Mockito.when(customerService.findById(notFoundCustomer.getId())).thenReturn(null);

        // when
        mockMvc.perform(MockMvcRequestBuilders.put(endpoint + "/{0}", notFoundCustomer.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .content(mapper.writeValueAsString(notFoundCustomer)))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        // then
        Mockito.verify(customerService).findById(notFoundCustomer.getId());
    }

    /* Test - DELETE: Delete customer by id
    Expected Response:
    Status = 200
    Body = Successfully deleted customer with id: d9f3ef82-2829-4d77-8f1b-e19c30d82bc6
    */
    @Test
    public void deleteCustomer() throws Exception {
        // given
        Mockito.doNothing().when(customerService).deleteById(may.getId());

        // when
        mockMvc.perform(MockMvcRequestBuilders.delete(endpoint + "/{0}", may.getId()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());

        // then
        Mockito.verify(customerService).deleteById(may.getId());
    }
}