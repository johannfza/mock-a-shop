package com.example.MockaShop.api;

import com.example.MockaShop.models.Customer;
import com.example.MockaShop.models.LineItem;
import com.example.MockaShop.models.Product;
import com.example.MockaShop.models.ShoppingCart;
import com.example.MockaShop.services.ShoppingCartService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ShoppingCartApiController.class)
public class ShoppingCartApiControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    ShoppingCartService shoppingCartService;

    @Autowired
    ObjectMapper mapper;

    Product product1;
    Product product2;
    Product product3;

    LineItem johnItem1;
    LineItem johnItem2;
    LineItem johnItem3;


    Customer john;
    Customer may;
    List<LineItem> johnItems;
    List<LineItem> mayItems;
    ShoppingCart johnCart;
    ShoppingCart mayCart;
    List<ShoppingCart> carts;

    String endpoint = "/api/shoppingcarts";

    /* Setup test data */
    @Before
    public void setup() throws JsonProcessingException {
        product1 = new Product(UUID.fromString("4dd76282-f38c-4bc9-8d0c-e3964849dd95"), "Product 1", "About Product 1", "", BigDecimal.valueOf(10));
        product2 = new Product(UUID.fromString("4299255c-8343-427d-a711-b54a68a42875"), "Product 2", "About Product 2", "", BigDecimal.valueOf(20));
        product3 = new Product(UUID.fromString("2eb0fcf0-4f9e-4278-b74e-df52d4dd6716"), "Product 3", "About Product 3", "", BigDecimal.valueOf(30));

        johnItem1 = new LineItem(UUID.fromString("0bbd0d87-769c-45f0-9d4c-5181290ef8a9"), 1, product1);
        johnItem2 = new LineItem(UUID.fromString("752dff14-534b-4586-9a67-bbab6d78551b"), 2, product2);
        johnItem3 = new LineItem(UUID.fromString("d8026ead-b765-4344-8b80-75353cb44a57"), 3, product3);

        johnItems = new ArrayList<>();
        johnItems.add(johnItem1);
        johnItems.add(johnItem2);
        johnItems.add(johnItem3);

        mayItems = new ArrayList<>();

        john = new Customer(UUID.fromString("6b83444c-5a9c-41ce-9785-53e4a582c4f9"), "John");
        may = new Customer(UUID.fromString("d9f3ef82-2829-4d77-8f1b-e19c30d82bc6"), "May");
        johnCart = new ShoppingCart(UUID.fromString("1f558ffc-2db0-4548-b0ab-9fe516c0d2f1"), johnItems, john);
        mayCart = new ShoppingCart(UUID.fromString("04deead6-ca4b-414a-bbd6-3ca3f706a515"), mayItems, may);

        carts = new ArrayList<ShoppingCart>();
        carts.add(johnCart);
        carts.add(mayCart);

    }

    /* Test - GET: Get all ShoppingCarts
    Expected Response:
    Status = 200
            // John's Cart
    Body = [{"id":"1f558ffc-2db0-4548-b0ab-9fe516c0d2f1",
                "items":[{"qty":1,"product":{"product_id":"4dd76282-f38c-4bc9-8d0c-e3964849dd95","name":"Product 1","description":"About Product 1","imgUrl":"","price":10},"item_id":"0bbd0d87-769c-45f0-9d4c-5181290ef8a9","subTotal":10},
                         {"qty":2,"product":{"product_id":"4299255c-8343-427d-a711-b54a68a42875","name":"Product 2","description":"About Product 2","imgUrl":"","price":20},"item_id":"752dff14-534b-4586-9a67-bbab6d78551b","subTotal":40},
                         {"qty":3,"product":{"product_id":"2eb0fcf0-4f9e-4278-b74e-df52d4dd6716","name":"Product 3","description":"About Product 3","imgUrl":"","price":30},"item_id":"d8026ead-b765-4344-8b80-75353cb44a57","subTotal":90}],
                         "customer":{"id":"6b83444c-5a9c-41ce-9785-53e4a582c4f9","access":2,"username":"John","shoppingCart":null},"empty":false,"total":140},
             // May's Cart
            {"id":"04deead6-ca4b-414a-bbd6-3ca3f706a515","items":[],"customer":{"id":"d9f3ef82-2829-4d77-8f1b-e19c30d82bc6","access":2,"username":"May","shoppingCart":null},"empty":true,"total":0}]

    // NOTE: @JsonIgnoreProperties("shoppingCart") was used to prevent infinite recursion.
    */
    @Test
    public void getAll() throws Exception {
        // given
        Mockito.when(shoppingCartService.getAll()).thenReturn(carts);

        // when
        mockMvc.perform(MockMvcRequestBuilders.get(endpoint))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.content().json(mapper.writeValueAsString(carts)))
                .andExpect(MockMvcResultMatchers.status().isOk());

        // then
        Mockito.verify(shoppingCartService).getAll();
    }

    /* Test - GET: Get cart by customer id
    Expected Response:
    Status = 200
    Body = {"id":"1f558ffc-2db0-4548-b0ab-9fe516c0d2f1",
            "items":[{"qty":1,"product":{"product_id":"4dd76282-f38c-4bc9-8d0c-e3964849dd95","name":"Product 1","description":"About Product 1","imgUrl":"","price":10},"item_id":"0bbd0d87-769c-45f0-9d4c-5181290ef8a9","subTotal":10},
                    {"qty":2,"product":{"product_id":"4299255c-8343-427d-a711-b54a68a42875","name":"Product 2","description":"About Product 2","imgUrl":"","price":20},"item_id":"752dff14-534b-4586-9a67-bbab6d78551b","subTotal":40},
                    {"qty":3,"product":{"product_id":"2eb0fcf0-4f9e-4278-b74e-df52d4dd6716","name":"Product 3","description":"About Product 3","imgUrl":"","price":30},"item_id":"d8026ead-b765-4344-8b80-75353cb44a57","subTotal":90}],
                           "customer":{"id":"6b83444c-5a9c-41ce-9785-53e4a582c4f9","access":2,"username":"John","shoppingCart":null},"empty":false,"total":140}
    */
    @Test
    public void getByCustomerId() throws Exception {
        // given
        Mockito.when(shoppingCartService.findByCustomerId(john.getId())).thenReturn(johnCart);

        // when
        mockMvc.perform(MockMvcRequestBuilders.get(endpoint + "/customer/{0}", john.getId()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.content().json(mapper.writeValueAsString(johnCart)))
                .andExpect(MockMvcResultMatchers.status().isOk());

        // then
        Mockito.verify(shoppingCartService).findByCustomerId(john.getId());
    }

    /* Test - GET: Get cart by cart id
    Expected Response:
    Status = 200
    Body = {"id":"1f558ffc-2db0-4548-b0ab-9fe516c0d2f1",
            "items":[{"qty":1,"product":{"product_id":"4dd76282-f38c-4bc9-8d0c-e3964849dd95","name":"Product 1","description":"About Product 1","imgUrl":"","price":10},"item_id":"0bbd0d87-769c-45f0-9d4c-5181290ef8a9","subTotal":10},
                    {"qty":2,"product":{"product_id":"4299255c-8343-427d-a711-b54a68a42875","name":"Product 2","description":"About Product 2","imgUrl":"","price":20},"item_id":"752dff14-534b-4586-9a67-bbab6d78551b","subTotal":40},
                    {"qty":3,"product":{"product_id":"2eb0fcf0-4f9e-4278-b74e-df52d4dd6716","name":"Product 3","description":"About Product 3","imgUrl":"","price":30},"item_id":"d8026ead-b765-4344-8b80-75353cb44a57","subTotal":90}],
                           "customer":{"id":"6b83444c-5a9c-41ce-9785-53e4a582c4f9","access":2,"username":"John","shoppingCart":null},"empty":false,"total":140}
    */
    @Test
    public void getByCartId() throws Exception {
        // given
        Mockito.when(shoppingCartService.findById(johnCart.getId())).thenReturn(Optional.of(johnCart));

        // when
        mockMvc.perform(MockMvcRequestBuilders.get(endpoint + "/{0}", johnCart.getId()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.content().json(mapper.writeValueAsString(johnCart)))
                .andExpect(MockMvcResultMatchers.status().isOk());

        // then
        Mockito.verify(shoppingCartService).findById(johnCart.getId());

    }


    /* Test - PUT: Clear Cart
    Expected Response:
    Status = 200
    Body = {"id":"1f558ffc-2db0-4548-b0ab-9fe516c0d2f1","items":[],"customer":{"id":"6b83444c-5a9c-41ce-9785-53e4a582c4f9","access":2,"username":"John","shoppingCart":null},"empty":true,"total":0}
    */
    @Test
    public void clearShoppingCart() throws Exception {
        // given
        ShoppingCart johnUpdatedCart = new ShoppingCart(johnCart.getId(), new ArrayList<>(), john);
        Mockito.when(shoppingCartService.findById(johnCart.getId())).thenReturn(Optional.of(johnCart));
        Mockito.when(shoppingCartService.clearCart(johnCart.getId())).thenReturn(johnUpdatedCart);

        // when
        mockMvc.perform(MockMvcRequestBuilders.put(endpoint + "/clear/{0}", johnCart.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .content(mapper.writeValueAsString(johnUpdatedCart)))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.content().json(mapper.writeValueAsString(johnUpdatedCart)))
                .andExpect(MockMvcResultMatchers.status().isOk());

        // then
        Mockito.verify(shoppingCartService).findById(johnCart.getId());
        Mockito.verify(shoppingCartService).clearCart(johnCart.getId());
    }

    /* Test - POST: Add new Item to cart by cart id
    Expected Response:
    Status = 200
    Body = {"id":"6b83444c-5a9c-41ce-9785-53e4a582c4f9","items":[{"qty":1,"product":{"product_id":"4dd76282-f38c-4bc9-8d0c-e3964849dd95","name":"Product 1","description":"About Product 1","imgUrl":"","price":10},"item_id":"0bbd0d87-769c-45f0-9d4c-5181290ef8a9","subTotal":10},{"qty":2,"product":{"product_id":"4299255c-8343-427d-a711-b54a68a42875","name":"Product 2","description":"About Product 2","imgUrl":"","price":20},"item_id":"752dff14-534b-4586-9a67-bbab6d78551b","subTotal":40},{"qty":3,"product":{"product_id":"2eb0fcf0-4f9e-4278-b74e-df52d4dd6716","name":"Product 3","description":"About Product 3","imgUrl":"","price":30},"item_id":"d8026ead-b765-4344-8b80-75353cb44a57","subTotal":90},{"qty":10,"product":{"product_id":"077858f4-44fc-445d-9535-31124b5a02ee","name":"Product 4","description":"About Product 4","imgUrl":"","price":10},"item_id":"0bbd0d87-769c-45f0-9d4c-5181290ef8a9","subTotal":100}],"customer":{"id":"6b83444c-5a9c-41ce-9785-53e4a582c4f9","access":2,"username":"John","shoppingCart":null},"empty":false,"total":240}
    */
    @Test
    public void addNewItemToCartByCartId() throws Exception {
        // given
        Product product4 = new Product(UUID.fromString("077858f4-44fc-445d-9535-31124b5a02ee"), "Product 4", "About Product 4", "", BigDecimal.valueOf(10));
        LineItem johnNewItem4 = new LineItem(UUID.fromString("0bbd0d87-769c-45f0-9d4c-5181290ef8a9"), 10, product4);

        List<LineItem> items = johnCart.getItems();
        items.add(johnNewItem4);

        ShoppingCart updatedShoppingCart = new ShoppingCart(johnCart.getId(), items, john);

        Mockito.when(shoppingCartService.addItem(Mockito.any(), Mockito.any())).thenReturn(updatedShoppingCart);

        // when
        mockMvc.perform(MockMvcRequestBuilders.post(endpoint + "/items/add/{0}", johnCart.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .content(mapper.writeValueAsString(johnNewItem4)))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.content().json(mapper.writeValueAsString(updatedShoppingCart)))
                .andExpect(MockMvcResultMatchers.status().isOk());

        // then
        Mockito.verify(shoppingCartService).addItem(Mockito.any(), Mockito.any());
    }

    /* Test - POST: Add new Item to cart by customer id
    Expected Response:
    Status = 200
    Body = {"id":"6b83444c-5a9c-41ce-9785-53e4a582c4f9","items":[{"qty":1,"product":{"product_id":"4dd76282-f38c-4bc9-8d0c-e3964849dd95","name":"Product 1","description":"About Product 1","imgUrl":"","price":10},"item_id":"0bbd0d87-769c-45f0-9d4c-5181290ef8a9","subTotal":10},{"qty":2,"product":{"product_id":"4299255c-8343-427d-a711-b54a68a42875","name":"Product 2","description":"About Product 2","imgUrl":"","price":20},"item_id":"752dff14-534b-4586-9a67-bbab6d78551b","subTotal":40},{"qty":3,"product":{"product_id":"2eb0fcf0-4f9e-4278-b74e-df52d4dd6716","name":"Product 3","description":"About Product 3","imgUrl":"","price":30},"item_id":"d8026ead-b765-4344-8b80-75353cb44a57","subTotal":90},{"qty":10,"product":{"product_id":"077858f4-44fc-445d-9535-31124b5a02ee","name":"Product 4","description":"About Product 4","imgUrl":"","price":10},"item_id":"0bbd0d87-769c-45f0-9d4c-5181290ef8a9","subTotal":100}],"customer":{"id":"6b83444c-5a9c-41ce-9785-53e4a582c4f9","access":2,"username":"John","shoppingCart":null},"empty":false,"total":240}
    */
    @Test
    public void addNewItemToCartByCustomerId() throws Exception {
        // given
        Product product4 = new Product(UUID.fromString("077858f4-44fc-445d-9535-31124b5a02ee"), "Product 4", "About Product 4", "", BigDecimal.valueOf(10));
        LineItem johnNewItem4 = new LineItem(UUID.fromString("0bbd0d87-769c-45f0-9d4c-5181290ef8a9"), 10, product4);

        List<LineItem> items = johnCart.getItems();
        items.add(johnNewItem4);

        ShoppingCart updatedShoppingCart = new ShoppingCart(johnCart.getId(), items, john);

        Mockito.when(shoppingCartService.addItemByCustomerId(Mockito.any(), Mockito.any())).thenReturn(updatedShoppingCart);

        // when
        mockMvc.perform(MockMvcRequestBuilders.post(endpoint + "/items/add/customer/{0}", john.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .content(mapper.writeValueAsString(johnNewItem4)))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.content().json(mapper.writeValueAsString(updatedShoppingCart)))
                .andExpect(MockMvcResultMatchers.status().isOk());

        // then
        Mockito.verify(shoppingCartService).addItemByCustomerId(Mockito.any(), Mockito.any());
    }
}