package com.example.MockaShop;

import com.example.MockaShop.api.CustomerApiControllerTest;
import com.example.MockaShop.api.ProductApiControllerTest;
import com.example.MockaShop.api.ShoppingCartApiControllerTest;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        CustomerApiControllerTest.class,
        ProductApiControllerTest.class,
        ShoppingCartApiControllerTest.class
})
@SpringBootTest
class MockAShopApiTest {

    @Test
    void contextLoads() {
    }

}
