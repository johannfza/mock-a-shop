package com.example.MockaShop;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

/* Test runner for Api Test suite
*  Run this to run all Api test
*  */
public class ApiTestRunner {
    public static void main(String[] args) {
        Result result = JUnitCore.runClasses(MockAShopApiTest.class);

        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }

        System.out.println(result.wasSuccessful());
    }
}