# Mock-A-Shop

Mock-A-Shop is a simple shopping web application that is written in Java. It is built upon the Spring Boot framework and consists of a Web Application and RESTful APIs.

## Getting Started

You'll find an instance of the project running on my [server](https://shop.treedays.cloud/). To spin up one locally just follow the instructions below.

### Prerequisites

You will need to have Docker installed to run this build.

- [Docker](https://docs.docker.com/get-docker/)

### Running the Application

Now that you have docker installed we can run the docker image straight from docker hub.
Pull the image from docker hub.

```
docker pull johannfza/mock-a-shop:1.0
```

There are 2 ways that docker allows you to run containers.

1. To run your docker container in the **foreground**
```
docker run --name mock-a-shop -p 8080:8080 johannfza/mock-a-shop:1.0
```

2. To run your docker container in the **background** 
```
docker run -d --name mock-a-shop -p 8080:8080 johannfza/mock-a-shop:1.0
```

Your container should now be up and running. Head to http://localhost:8080/ to check it out! 

## Cleaning up

You probably won't want it running or staying in your computer after this. So when you're done, I've included steps to remove the image and the container. But for now you can [skip](#restapi-endpoints) this.

List all docker containers

```
docker ps -a 
```
Stop container if it's still running. 
```
docker stop mock-a-shop
```

The container is still in your system. To remove it just run:
```
docker rm mock-a-shop
```
The run the following command to check that the container is removed
```
docker ps -a 
```
Remove the image
```
docker rmi johannfza/mock-a-shop:1.0
docker rmi openjdk:15-jdk-alpine
```
The run the following command to check that the image is removed
```
docker images
```

Yay! it's all clean now! 

## RESTApi Endpoints

If you would like to see the **class diagram** for the project it's [here](class.png).

The application comes with several endpoints. I've broken them down into [curl](https://curl.haxx.se/) commands so all you need is the terminal! :smiley: Lets go! 

- [Customers API](#customers-api)
- [Products API](#products-api)
- [Shopping Cart API](#shopping-carts-api)

If you would like to test them on with my server just change "http://localhost:8080" to "https://shop.treedays.cloud"

### ReSeed

All these commands should work on a fresh instance of the project. If you would like to re-seed the database just 'time-travel'.
```
curl -i http://localhost:8080/seed/timetravel 
```

### Customers API

### GET: Get all customers

```
curl -i http://localhost:8080/api/customers
```

### GET: Get customer by id

```
curl -i http://localhost:8080/api/customers/6b83444c-5a9c-41ce-9785-53e4a582c4f9

### Getting John's details

```

### POST: Create customer

```
curl -i -X POST -H "Content-Type: application/json" \
-d '{"username":"New Customer"}' \
http://localhost:8080/api/customers

### Creating a new Customer 'New Customer'

```

### PUT: Update customer

```
curl -i -X PUT -H "Content-Type: application/json" \
-d '{"id":"6b83444c-5a9c-41ce-9785-53e4a582c4f9", "username":"Johns Updated Name"}' \
http://localhost:8080/api/customers/6b83444c-5a9c-41ce-9785-53e4a582c4f9

### Updating John's name 

```

### DELETE: Delete customer

```
curl -i -X DELETE http://localhost:8080/api/customers/04b0114f-db09-40b1-b451-d960eb0eba64

### Deleting Ahmad! This will also delete the customer's shopping cart and it's items.
```
### Products API

### GET: Get all products

```
curl -i http://localhost:8080/api/products
```

### GET: Get product by id

```
curl -i http://localhost:8080/api/products/4dd76282-f38c-4bc9-8d0c-e3964849dd95

### Getting 'Californication Vinyl by RHCP' details

```

### POST: Create product

```
curl -i -X POST -H "Content-Type: application/json" \
-d '{"name":"New Product", "description":"About New Product", "imgUrl":"", "price":10.50}' \
http://localhost:8080/api/products

### Creating 'New Product'

```

### PUT: Update product

```
curl -i -X PUT -H "Content-Type: application/json" \
-d '{"id":"4dd76282-f38c-4bc9-8d0c-e3964849dd95", "name":"Californication Vinyl by RHCP", "description":"New Description", "imgUrl":"", "price":1000000000}' \
http://localhost:8080/api/customers/4dd76282-f38c-4bc9-8d0c-e3964849dd95

### Updating description and price of 'Californication Vinyl by RHCP'

```

### DELETE: Delete product

```
curl -i -X DELETE http://localhost:8080/api/products/4dd76282-f38c-4bc9-8d0c-e3964849dd95

### Deleting 'Californication Vinyl by RHCP', this will also delete items from the shopping cart
```

### Shopping Carts API

### GET: Get all shopping carts

```
curl -i http://localhost:8080/api/shoppingcarts
```

### GET: Get cart by cart id

```
curl -i http://localhost:8080/api/shoppingcarts/ff371858-4be8-408a-812c-6d63886ab2e6

### Getting John's shopping cart

```

### GET: Get cart by customer id

```
curl -i http://localhost:8080/api/shoppingcarts/customer/6b83444c-5a9c-41ce-9785-53e4a582c4f9

### Getting John's shopping cart using John's Id
```

### POST: Add item to cart

```
curl -i -X POST -H "Content-Type: application/json" \
-d '{"qty":10, "product": {"product_id":"6c295528-1172-45b1-a3b7-731ace6484fe"}}' \
http://localhost:8080/api/shoppingcarts/items/add/898a7400-4201-4db7-8024-8d4791365f63

### Adding 10 'Dark Side of the Moon Vinyl by Pink Floyd' to May's cart

```

### POST: Add item to cart by customer id

```
curl -i -X POST -H "Content-Type: application/json" \
-d '{"qty":8, "product": {"product_id":"2eb0fcf0-4f9e-4278-b74e-df52d4dd6716"}}' \
http://localhost:8080/api/shoppingcarts/items/add/customer/d9f3ef82-2829-4d77-8f1b-e19c30d82bc6

### Adding 8 'Random Access Memories Vinyl by Daft Punk' to May's cart using May's Id
```

### PUT: Clear shopping cart

```
curl -i -X PUT http://localhost:8080/api/shoppingcarts/clear/898a7400-4201-4db7-8024-8d4791365f63

### Clearing May's cart

```

### DELETE: Remove Item from cart

```
curl -i -X DELETE http://localhost:8080/api/shoppingcarts/items/301e7d16-c6a1-467f-9375-b5b903115195/delete/898a7400-4201-4db7-8024-8d4791365f63

### Removed 'Dark Side of the Moon Vinyl by Pink Floyd' from John's cart

```


## Testing with JUnit and Mockito

Test files have also been included [here](https://gitlab.com/johannfza/mock-a-shop/-/tree/master/src/test/java/com/example/MockaShop/api) in the project for testing the API. Just open up this project in your fav Java IDE and run them. I've also included a [ApiTestRunner](https://gitlab.com/johannfza/mock-a-shop/-/blob/master/src/test/java/com/example/MockaShop/ApiTestRunner.java) class for Suite testing.

## Built With

* [Spring Boot](https://spring.io/projects/spring-boot) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [Thymeleaf](https://www.thymeleaf.org/) - Server-side Java template engine
* [NGINX](https://www.nginx.com/) - Web Server
* [Docker](https://docs.docker.com/) - Container 
* [Linode](https://www.linode.com/) - Cloud Hosting

## Author

* **Johann Fong** 

## Acknowledgments

* John Yu and Team 9


